<?php

function eve_get_content_types() {
  $content_types = array(
    'eve_account'   => array(
      'type'      => 'eve_account',
      'name'      => 'Eve API account',
      'base'      => 'node_content',
      'description' => 'Create account using EVE API key',
      'has_body'  => FALSE,
    ),
  );
  return $content_types;
}

function eve_get_fields() {
  return array(
    'user_id' => array(
      'field_name'    => 'user_id',
      'type'          => 'number_integer',
      'cardinality'   => 1,
    ),
    'api_key' => array(
      'field_name'    => 'api_key',
      'type'          => 'text',
      'cardinality'   => 1,
      'settings'    => array(
        'max_length'    => 64,
      ),
    ),
    // This field will not be user editable
    'character_id' => array(
      'field_name'  => 'character_id',
      'type'          => 'number_integer',
      'cardinality'   => 3,
    ),
  );
}

/**
 * Implements hook_install()
 */
function eve_install() {
  // Create custom node types
  foreach (eve_get_content_types() as $content_type) {
    $content_type = node_type_set_defaults($content_type);
    node_type_save($content_type);
  }

  // Create eve fields
  foreach (eve_get_fields() as $eve_field) {
    field_create_field($eve_field);
  }

  $eve_instances = array(
    'user_id' => array(
      'field_name'  => 'user_id',
      'label'       => 'User ID',
      'required'    => TRUE,
      'widget'    => array(
        'weight' => 1,
      ),
    ),
    'api_key' => array(
      'field_name'  => 'api_key',
      'label'       => 'API Key',
      'required'    => TRUE,
      'widget'    => array(
        'weight' => 2,
      ),
    ),
    'character_id' => array(
      'field_name'  => 'character_id',
      'label'       => 'Character list',
    ),
  );
  // attach above instances to eve_account type
  foreach ($eve_instances as $eve_instance) {
    $eve_instance['object_type'] = 'node';
    $eve_instance['bundle'] = 'eve_account';
    field_create_instance($eve_instance);
  }

  // Some form api fields are not available during hook_form_alter unless our module comes last
  db_update('system')
    ->fields(array(
      'weight' => 1,
    ))
    ->condition('name', 'eve')
    ->execute();
}

/**
 * Implements hook_uninstall()
 */
function eve_uninstall() {
  // Attempt to thoroughly remove every node type, field and field instances we introduced.
  // This enables the use of devel module reinstall for rapid development
  // This is less useful (and should not be used) at post site launch stage
  foreach (array_keys(eve_get_content_types()) as $content_type) {
    // load all this content type's existing nodes and delete them.
    foreach (node_load_multiple(array(), array('type' => $content_type)) as $node) {
      $nids[] = $node->nid; // do it all in one batch for faster results.
    }
    if (isset($nids)) {
      node_delete_multiple($nids);
    }

    // load all this content type's instances and remove them.
    $instances = field_info_instances('node', $content_type);
    foreach ($instances as $instance_name => $instance) {
      field_delete_instance($instance);
    }

    node_type_delete($content_type);
  }

  foreach (array_keys(eve_get_fields()) as $field) {
    // there's no mass delete for fields.
    field_delete_field($field);
  }
  // purge deleted data, fields, and instances.
  field_purge_batch(1000);
}

